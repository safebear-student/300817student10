package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 30/08/2017.
 */
public class Test01_Login  extends BaseTest{
    @Test
    public void testLogin(){
        //step1 confirm we are on the welcome page
        assertTrue(welcomePage.checkCorrectPage());

        //step2 click on the login link and login page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));

        //step3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }



}
