package com.safebear.app;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import pages.FramesPage;
import pages.LoginPage;
import pages.UserPage;
import pages.WelcomePage;
import utils.Utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 30/08/2017.
 */
public class BaseTest {
    WebDriver driver;
    Utils utility;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;
    FramesPage framesPage;

    @Before
    public void setUp() throws MalformedURLException {
        //driver=new ChromeDriver();
        DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);
        framesPage = new FramesPage(driver);
        utility = new Utils();
        assertTrue(utility.navigateToWebsite(driver));
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
